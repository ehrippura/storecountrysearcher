//
//  Function.swift
//  StoreCountrySearch
//
//  Created by Wayne Lin on 2016/7/28.
//  Copyright © 2016年 Tzu-Yi Lin. All rights reserved.
//

import Foundation

private let operationQueue = OperationQueue()

private let session = URLSession(
    configuration: URLSessionConfiguration.default,
    delegate: nil,
    delegateQueue: operationQueue
)

func scan(searchModel: SearchModel) -> [String] {

    var hit: [String] = []
    let group = DispatchGroup()

    for countryCode in countryCodes {

        let query = queryURL(appName: search.searchKeyword, countryCode: countryCode)
        let request = URLRequest(url: query)

        group.enter()
        let task = session.dataTask(with: request) { (data, response, error) -> Void in

            defer {
                group.leave()
            }

            guard let data = data else {
                return
            }

            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])

                if let obj = jsonObject as? [String : AnyObject], let results = obj["results"] as? [[String : AnyObject]] {

                    let entities = results.map { StoreEntity(dictionary: $0) }
                    if contains(appID: search.searchAppID, entities: entities) {
                        hit.append(countryCode)
                    }
                }
            } catch _ { }

            print("scan \(countryCode) complete")
        }

        task.resume()
    }

    _ = group.wait(timeout: DispatchTime.distantFuture)

    return hit
}

private func queryURL(appName: String, countryCode: String) -> URL {

    var urlComponent = URLComponents(string: "https://itunes.apple.com/search")!

    let urlQueryItem = [
        URLQueryItem(name: "entity", value: "software"),
        URLQueryItem(name: "limit", value: "5"),
        URLQueryItem(name: "term", value: appName),
        URLQueryItem(name: "country", value: countryCode)
    ]

    urlComponent.queryItems = urlQueryItem

    return urlComponent.url!
}

private func contains(appID: UInt, entities: [StoreEntity]) -> Bool {

    for entity in entities {
        if entity.trackID == appID {
            return true
        }
    }

    return false
}
